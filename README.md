# Criando um Pipeline de Deploy com GitLab e Kubernetes

Este é mais um projeto da DIO.
Neste projeto será realizado um deploy de uma aplicação completa com frontend, backend e database mysql. No desenvolvimento do projeto serão criadas as imagens dos containeres e serviços necessários no kubernetes para que a aplicação esteja pronta para produção.

Foi criado um cluster na nuvem GCP e criada uma imagem personalizada para a aplicação. A imagem foi enviada para o docker hub.

Foi criado também um script .sh para executar os comandos para subir a aplicação, sem que fosse necessário digitar todos os comandos do kubectl.